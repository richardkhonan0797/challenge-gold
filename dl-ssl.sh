#!/bin/sh
cd ./nginx/ssl/
wget https://gitlab.com/roboticpuppies/ssl-certificate/-/raw/main/cert1.pem -O cert.pem
wget https://gitlab.com/roboticpuppies/ssl-certificate/-/raw/main/chain1.pem -O chain.pem
wget https://gitlab.com/roboticpuppies/ssl-certificate/-/raw/main/privkey1.pem -O private.pem